<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dokter;
use App\Pasien;
use App\Dokter_pasien;

class DokterPasienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dpsindex()
    {
        $daftardokter=Dokter::all();
        $daftarpasien=Pasien::all();
        return view('dokterhaspasien.form_list_dps',compact('daftardokter','daftarpasien'));
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function dpsstore(Request $request)
    {        
        $pasienarr=$request['nama_pasien'];
        $dokterid= $request['nama_dokter'];        
        $waktu_periksa=$request['date'];        
        $getpasien=Pasien::find($pasienarr);
        if ($request['token']==$getpasien->token) {
        $dokteriddb=Dokter::find($dokterid);
        $getnamadokter = $dokteriddb->nama_dokter;
        $getspesialisasi = $dokteriddb->specialist;
        $dokteriddb->dokter_pasien()->attach($dokterid, ['pasien_id' => $pasienarr,"waktu_periksa"=>$waktu_periksa,
    "nama_dokter"=>$getnamadokter,"spesialisasi"=>$getspesialisasi]);

        return redirect('/dokterhaspasien')->with('success','jadwal periksa berhasil disimpan');    
        }else{
        return redirect('/dokterhaspasien')->with('gagal','Token salah, silahkan hubungi admin klinik');        
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showlistpasien()
    {
        $alldokter=Dokter::all();
        $dokter=Dokter::find(1);
        return view('dokterhaspasien.list_pasien',compact('dokter','alldokter'));
    }


    public function showlistpasienbydokter(Request $request)
    {   
            $id_dokter=$request['id_dokter'];
            $alldokter=Dokter::all();            
            $dokter=Dokter::find($id_dokter);
            $selecteddokter=$dokter->nama_dokter;
            return view('dokterhaspasien.list_pasien',compact('dokter','alldokter','selecteddokter'));
   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
