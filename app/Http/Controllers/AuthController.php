<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('page.register');
    }
    public function welcomes(Request $request) {
        // dd($request);
        // dd($request['firstname']);
        // dd($request->firstname);
        // dd($request->all()); //Menampilkan semua request yg di inputkan

        $firstname = $request["firstname"];
        $lastname = $request->lastname;
        // dd($firstname, $lastname);
        // return "$firstname"."$lastname";

        return view('page.welcomes', compact('firstname', 'lastname'));
        //compact berguna untuk mem-passing data yang lebih dari 1 secara sekaligus tanpa harus menggunakan fungsi with() kedalam view yang diarahkan
        //return view('page.welcomes')->with('firstname', $firstname);
    }
}
