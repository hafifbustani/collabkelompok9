<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dokter;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File; 

class DokterController extends Controller
{
    public function create() {
        return view('dokter.create');
    }

    public function post(Request $request){
        // $dokter = new dokter;
        // $dokter->nama_dokter=$request['nama_dokter'];
        // $dokter->specialist=$request['specialist'];
        // $dokter->save();
        
        if(!empty($request->file('foto'))) {
            $gambar = $request->foto;
            $gambar2 = time().'-'.$gambar->getClientOriginalName();        
            $gambar->move(public_path()."/uploads/", $gambar2);
            $dokter = new dokter; //menggunakan model
            $dokter->nama_dokter=$request['nama_dokter'];
            $dokter->specialist=$request['specialist'];
            $dokter->urlfoto=$gambar2;                
            $dokter->save();
        } else {
            $dokter = new dokter; //menggunakan model
            $dokter->nama_dokter=$request['nama_dokter'];
            $dokter->specialist=$request['specialist'];                               
            $konten->save();
        }

        return redirect('/dokter/create')->with('success',' data dokter berhasil disimpan');
    }

    public function store(Request $request) {
        // dd($request->all());
        $request->validate([
            'nama_dokter' => 'required|unique:dokter|max:255',
            'specialist' => 'required',
        ]);
        $query = DB::table('dokter')->insert([
            'nama_dokter' => $request['nama_dokter'],
            'specialist' => $request['specialist'],
            ]);
        return redirect('/dokter')->with('status', 'Add Data Success!');
    }
    
    public function index() {
        $dokter = DB::table('dokter')->get(); // Mirip dg SELECT * FROM pd SQL (Mengambil semua data)
        // dd($dokter); 
        return view('dokter.index', compact('dokter'));
    }

    public function frontenddokter(){
        $dokter = DB::table('dokter')->get(); // Mirip dg SELECT * FROM pd SQL (Mengambil semua data)
        return view('doctor', compact('dokter'));
    }
    public function indexhome(){
        $dokter = DB::table('dokter')->get(); // Mirip dg SELECT * FROM pd SQL (Mengambil semua data)
        return view('index', compact('dokter'));
    }

    public function show($id) {
        $query = dokter::find($id);
        // dd($query);
        return view('dokter.show', compact('query'));
    }
    public function edit($id) {
        $query = dokter::find($id);
        return view('dokter.edit', compact('query'));
    }
    public function update($id, Request $request) {
        $request->validate([
            'nama_dokter' => 'required|max:255',
            'specialist' => 'required',
            // 'gender' => 'required'
        ]);

        $query = Dokter::find($id);
        $query->nama_dokter = $request->nama_dokter;
        $query->specialist = $request->specialist;
        $query->save();
        

        return redirect('/dokter')->with('status', 'Edit Data Success!');
    } 
    public function destroy($id) {
        dokter::destroy($id);
        return redirect('/dokter')->with('status', 'Delete Data Success!');

    }  
}