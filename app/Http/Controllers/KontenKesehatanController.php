<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KontenKesehatan;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File; 

class KontenKesehatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $konten = KontenKesehatan::all();//select data
        return view('kontenkesehatan.daftarkontenshow', compact('konten')); 
    }

    public function frontendkonten()
    {
        $konten = KontenKesehatan::all();
        return view('blog', compact('konten'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kontenkesehatan.form_input_konten_kesehatan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul'=> 'required|unique:konten_kesehatan',
            'isi'=> 'required'
        ]);

        if(!empty($request->file('foto'))) {
                $gambar = $request->foto;
                $gambar2 = time().'-'.$gambar->getClientOriginalName();    
                $gambar->move(public_path()."/uploads/", $gambar2);
                $konten = new KontenKesehatan; //menggunakan model
                $konten->judul= $request['judul'];
                $konten->isi= $request['isi'];
                $konten->summary= $request['summary'];
                $konten->urlfoto=$gambar2;                
                $konten->save();
            } else {
                $konten = new KontenKesehatan; //menggunakan model
                $konten->judul= $request['judul'];
                $konten->isi= $request['isi'];
                $konten->summary= $request['summary'];                                
                $konten->save();
            }
        
        return redirect('/kontenkesehatanshow')->with('success','konten berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $konten= KontenKesehatan::find($id); //get 1 index menggunakan model
        return view('kontenkesehatan.kontenshow', compact('konten'));
    }
    public function fshow($id)
    {
        $konten= KontenKesehatan::find($id); //get 1 index menggunakan model
        return view('blogdetails', compact('konten'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $konten= KontenKesehatan::find($id);
        return view('kontenkesehatan.konten_kesehatan_edit',compact('konten'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
         $request->validate([
             'judul'=> 'required',
             'isi'=> 'required'
         ]);

        // $gambar = $request->foto;

        // $gambar2 = time().'-'.$gambar->getClientOriginalName();    
        // $gambar->move(public_path()."/uploads/", $gambar2);

        $konten = KontenKesehatan::find($id); //pake MODEL
        $konten->judul=$request['judul'];
        $konten->isi=$request['isi'];
        $konten->summary= $request['summary'];

        if(!empty($request['foto'])){
            if($konten->urlfoto==$request['foto']){
                $konten->save();
            } else{
                $gambar_lama=$konten->urlfoto;
                File::delete(public_path()."/uploads/".$gambar_lama);
                $gambar = $request->foto;
                $gambar2 = time().'-'.$gambar->getClientOriginalName();    
                $gambar->move(public_path()."/uploads/", $gambar2);
                $konten->urlfoto=$gambar2;
                $konten->save();
            }                
        }else{
            $konten->save();
        }
        

        return redirect('/kontenkesehatanshow')->with('success','berhasil update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $konten= KontenKesehatan::find($id);
        $gambar=$konten->urlfoto;
        if(!empty($gambar)){
            File::delete(public_path()."/uploads/".$gambar);
        }
        
        $konten->delete();
        return redirect('/kontenkesehatanshow')->with('success','berhasil dihapus');
    }
    public function destroy2($id)
    {
    $konten= KontenKesehatan::find($id);
    $gambar=$konten->urlfoto;
        if(!empty($gambar)){
            File::delete(public_path()."/uploads/".$gambar);
        }
    $konten->delete();    
    return back()->with('success','Konten deleted successfully');
    }

}
