<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pasien;


class PasienController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth')->only(['create'.'edit','store']);
        $this->middleware('auth')->except(['index']);
    }
    public function create() {
        return view('pasien.create');
    }

    public function store(Request $request) {

        $request->validate([
            'nama_pasien' => 'required|unique:pasien|max:255',
            'umur' => 'required',
            // 'gender' => 'required'
        ]);
        // $query = DB::table('pasien')->insert([
        //      'nama_pasien' => $request['nama_pasien'],
        //      'umur' => $request['umur'],
        //      'gender' => $request['gender'],
        //      'token'=> $request['token']
        //      ]);
            $query= new Pasien;
            $query->nama_pasien = $request->nama_pasien;
            $query->umur = $request->umur;
            $query->gender = $request->gender;
            $query->token = $request->token;
            $query->save();

        return redirect('/pasien')->with('status', 'Add Data Success!');
    }

    public function index() {
        $pasien = Pasien::all();
        // dd($pasien); 
        return view('pasien.index', compact('pasien'));
    }

    public function show($id) {
        $query = Pasien::find($id);
        // dd($query);
        return view('pasien.show', compact('query'));
    }

    public function edit($id) {
        $query = Pasien::find($id);
        // dd($query);
        return view('pasien.edit', compact('query'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'nama_pasien' => 'required|max:255',
            'umur' => 'required',
            // 'gender' => 'required'
        ]);
       
        $query = Pasien::find($id);
        $query->nama_pasien = $request->nama_pasien;
        $query->umur = $request->umur;
        $query->gender = $request->gender;
        $query->token = $request->token;
        $query->save();

        return redirect('/pasien')->with('status', 'Edit Data Success!');
    }

    public function destroy($id) {
        Pasien::destroy($id);
        return back()->with('status','Pasien deleted successfully');
    }
}
