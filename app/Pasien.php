<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    protected $table = 'pasien';
    protected $guarded = []; 
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    } 
    // public function dokter_pasien(){
    //     return $this->belongsToMany('App\Pasien'); //many to many appointment (Hafif)
    // }
}
