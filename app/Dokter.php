<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokter extends Model
{
    protected $table = 'dokter';

    public function dokter_pasien(){
        return $this->belongsToMany('App\Pasien'); //many to many appointment (Hafif)
    }
    public function pasien(){
        return $this->belongsToMany('App\Pasien','dokter_pasien','dokter_id','pasien_id')->withPivot('waktu_periksa','nama_dokter','spesialisasi');
    }
}
