<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Docmed</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="{{ asset('template/css/bootstrap.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('template/css/owl.carousel.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('template/css/magnific-popup.css') }} ">
    <link rel="stylesheet" href="{{ asset('template/css/font-awesome.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('template/css/themify-icons.css') }} ">
    <link rel="stylesheet" href="{{ asset('template/css/nice-select.css') }} ">
    <link rel="stylesheet" href="{{ asset('template/css/flaticon.css') }} ">
    <link rel="stylesheet" href="{{ asset('template/css/gijgo.css') }} ">
    <link rel="stylesheet" href="{{ asset('template/css/animate.css') }} ">
    <link rel="stylesheet" href="{{ asset('template/css/slicknav.css') }} ">
    <link rel="stylesheet" href="{{ asset('template/css/style.css') }} ">
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->
</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- header-start -->
    <header>
        <div class="header-area ">
            <div class="header-top_area">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-md-6 ">
                            <div class="social_media_links">
                                <a href="#">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-6 col-md-6">
                            <div class="short_contact_list">
                                <ul>
                                    <li><a href="#"> <i class="fa fa-envelope"></i> info@docmed.com</a></li>
                                    <li><a href="#"> <i class="fa fa-phone"></i> 160160</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sticky-header" class="main-header-area">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-3 col-lg-2">
                            <div class="logo">
                                <a href="index.html">
                                    <img src="{{ asset('template/img/logo.png') }}" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-7">
                            <div class="main-menu  d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a class="active" href="/">home</a></li>
                                        <li><a href="/departement">Department</a></li>
                                        <li><a href="/blog">blog </a>
                                            
                                        </li>
                                        <li><a href="/doctors">Doctors</a></li>
                                        <li><a href="/contact">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                            <div class="Appointment">
                                <div class="book_btn d-none d-lg-block">
                                    <a class="" href="/dokterhaspasien">Make an Appointment</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-end -->

    <!-- bradcam_area_start  -->
    <div class="bradcam_area breadcam_bg_2 bradcam_overlay">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text">
                        <h3>Doctors</h3>
                        <p><a href="index.html">Home /</a> Doctors</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- bradcam_area_end  -->

    <!-- expert_doctors_area_start -->
    <div class="expert_doctors_area doctor_page">
        <div class="container">
            <div class="row">
                @forelse ($dokter as $item =>$value)
                <div class="col-md-6 col-lg-3">
                    <div class="single_expert mb-40">
                        <div class="expert_thumb">
                            <img src="{{ asset('uploads/'.$value->urlfoto) }}" alt="">
                        </div>
                        <div class="experts_name text-center">
                            <h3>dokter - {{$value->nama_dokter}}</h3>
                            <span>{{$value->specialist}}</span>
                        </div>
                    </div>
                </div>                    
                @empty
                <div class="col-md-6 col-lg-3">
                    <div class="single_expert mb-40">
                        <div class="expert_thumb">
                            <img src="{{ asset('template/img/experts/1.png') }}" alt="">
                        </div>
                        <div class="experts_name text-center">
                            <h3>Ahmad Mujib</h3>
                            <span>Spesialis THT</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="single_expert mb-40">
                        <div class="expert_thumb">
                            <img src="{{ asset('template/img/experts/2.png') }}" alt="">
                        </div>
                        <div class="experts_name text-center">
                            <h3>Syarifudin Ihsan</h3>
                            <span>Spesialis Kandungan</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="single_expert mb-40">
                        <div class="expert_thumb">
                            <img src="{{ asset('template/img/experts/3.png') }}" alt="">
                        </div>
                        <div class="experts_name text-center">
                            <h3>Anton Suhar</h3>
                            <span>Spesialis Neurologit</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="single_expert mb-40">
                        <div class="expert_thumb">
                            <img src="{{ asset('template/img/experts/4.png') }}" alt="">
                        </div>
                        <div class="experts_name text-center">
                            <h3>Alan Daniel</h3>
                            <span>Spesialis Mata</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="single_expert mb-40">
                        <div class="expert_thumb">
                            <img src="{{ asset('template/img/experts/6.png') }}" alt="">
                        </div>
                        <div class="experts_name text-center">
                            <h3>Patihul Husni</h3>
                            <span>Spesialis Kandungant</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="single_expert mb-40">
                        <div class="expert_thumb">
                            <img src="{{ asset('template/img/experts/7.png') }}" alt="">
                        </div>
                        <div class="experts_name text-center">
                            <h3>Meta Syafitri</h3>
                            <span>Spesialis Gizi Anak</span>
                        </div>
                    </div>
                </div>    
                @endforelse
            </div>
        </div>
    </div>
    <!-- expert_doctors_area_end -->

    <!-- testmonial_area_start -->
    <div class="testmonial_area">
        <div class="testmonial_active owl-carousel">
            <div class="single-testmonial testmonial_bg_1 overlay2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-10 offset-xl-1">
                            <div class="testmonial_info text-center">
                                <div class="quote">
                                    <i class="flaticon-straight-quotes"></i>
                                </div>
                                <p>Rumah Sakit yang berkomitmen untuk selalu hadir dalam menolong <br>
                                sesama melalui pelayanan medis terpadu dan fasilitas yang memberikan <br>
                                kemudahan untuk kehidupan yang lebih sehat <br>
                                <div class="testmonial_author">
                                    <h4>Ratih Fitri</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-testmonial testmonial_bg_2 overlay2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-10 offset-xl-1">
                            <div class="testmonial_info text-center">
                                <div class="quote">
                                    <i class="flaticon-straight-quotes"></i>
                                </div>
                                <p>Menurut saya, pelayanan yang diberikan membangun rasa aman dan tenang.
                                <br>Kedepannya semoga Dipertahankan yang baiknya dan semakin ditingkatkan pelayanannya.
                                </p>
                                <div class="testmonial_author">
                                    <h4>Adi Wijaya</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-testmonial testmonial_bg_1 overlay2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-10 offset-xl-1">
                            <div class="testmonial_info text-center">
                                <div class="quote">
                                    <i class="flaticon-straight-quotes"></i>
                                </div>
                                <p>Terima Kasih untuk pelayanannya RS Hosana Medica Lippo Cikarang,
                                <br>semoga semakin maju dan bermanfaat untuk masyarakat.
                                </p>
                                <div class="testmonial_author">
                                    <h4>Vita Karim</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- testmonial_area_end -->

    <!-- business_expert_area_start  -->
    <div class="business_expert_area">
        <div class="business_tabs_area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <ul class="nav" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                            aria-selected="true">Pelayanan Cepat</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                            aria-selected="false">Medical Check Up</a>
                            </li>


                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"
                            aria-selected="false">Tanggap Covid</a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
        <div class="container">
            <div class="border_bottom">
                    <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                
                                    <div class="row align-items-center">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="business_info">
                                                    <div class="icon">
                                                        <i class="flaticon-first-aid-kit"></i>
                                                    </div>
                                                    <h3>Layanan 24 jam</h3>
                                                    <p>Dilengkapi dengan berbagai layanan yang siap melayani Anda selama 24 jam tanpa henti. Ada layanan Laboratorium yang melayani permintaan pemeriksaan dari internal maupun rujukan dari luar Ciputra Hospital dan ada juga Instalasi Gawat Darurat (IGD), yang siap melayani pasien dengan ketersediaan dokter IGD dan paramedik setiap saat.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-md-6">
                                                <div class="business_thumb">
                                                    <img src="{{ asset('template/img/department/2.png') }}" alt="">
                                                </div>
                                            </div>
                                        </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="row align-items-center">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="business_info">
                                                    <div class="icon">
                                                        <i class="flaticon-first-aid-kit"></i>
                                                    </div>
                                                    <h3>Pengecekan Umum </h3>
                                                    <p>Mencegah lebih baik daripada mengobati. Kesehatan Anda adalah sesuatu yang tak ternilai harganya. Kesehatan harus Anda jaga sebaik mungkin sebelum penyakit menggerogoti berbagai aspek kehidupan Anda. Sebagai langkah pencegahan, lakukanlah Medical Check Up (MCU) untuk mengetahui kondisi kesehatan Anda, sekaligus mendeteksi suatu penyakit sejak dini.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-md-6">
                                                <div class="business_thumb">
                                                    <img src="{{ asset('template/img/about/business.png') }}" alt="">
                                                </div>
                                            </div>
                                        </div>
                            </div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    <div class="row align-items-center">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="business_info">
                                                    <div class="icon">
                                                        <i class="flaticon-first-aid-kit"></i>
                                                    </div>
                                                    <h3>Covid Drive Thrue</h3>
                                                    <p>Dalam upaya memenuhi kebutuhan pelayanan pemeriksaan Swab PCR COVID-19 yang cepat, mudah, dan relatif aman, RSUI menyediakan layanan drive thru, dengan melakukan pendaftaran dan pengisian form melalui pesan WhatsApp maksimal H-1, sebelum jam 15.00.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-md-6">
                                                <div class="business_thumb">
                                                    <img src="{{ asset('template/img/department/4.png') }}" alt="">
                                                </div>
                                            </div>
                                        </div>
                            </div>
                          </div>
            </div>
        </div>
    </div>
    <!-- business_expert_area_end  -->

    <!-- Emergency_contact start -->
    <div class="Emergency_contact">
        <div class="conatiner-fluid p-0">
            <div class="row no-gutters">
                <div class="col-xl-6">
                    <div class="single_emergency d-flex align-items-center justify-content-center emergency_bg_1 overlay_skyblue">
                        <div class="info">
                            <h3>Emergency 24 jam</h3>
                            <p>Layanan Cepat Tanggap Darurat</p>
                        </div>
                        <div class="info_button">
                            <a href="#" class="boxed-btn3-white">021 378 4673 467</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="single_emergency d-flex align-items-center justify-content-center emergency_bg_2 overlay_skyblue">
                        <div class="info">
                            <h3>Make an Online Appointment</h3>
                            <p>Untuk melakukan Pendaftaran melalui website</p>
                        </div>
                        <div class="info_button">
                            <a href="/dokterhaspasien" class="boxed-btn3-white">Make an Appointment</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Emergency_contact end -->

<!-- footer start -->
    <footer class="footer">
            <div class="footer_top">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-4 col-md-6 col-lg-4">
                            <div class="footer_widget">
                                <div class="footer_logo">
                                    <a href="#">
                                        <img src="{{ asset('img/footer_logo.png') }}" alt="">
                                    </a>
                                </div>
                                <p>
                                        Firmament morning sixth subdue darkness 
                                        creeping gathered divide.
                                </p>
                                <div class="socail_links">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <i class="ti-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="ti-twitter-alt"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-instagram"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
    
                            </div>
                        </div>
                        <div class="col-xl-2 offset-xl-1 col-md-6 col-lg-3">
                            <div class="footer_widget">
                                <h3 class="footer_title">
                                        Departments
                                </h3>
                                <ul>
                                    <li><a href="#">Eye Care</a></li>
                                    <li><a href="#">Skin Care</a></li>
                                    <li><a href="#">Pathology</a></li>
                                    <li><a href="#">Medicine</a></li>
                                    <li><a href="#">Dental</a></li>
                                </ul>
    
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-lg-2">
                            <div class="footer_widget">
                                <h3 class="footer_title">
                                        Useful Links
                                </h3>
                                <ul>
                                    <li><a href="#">About</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">About</a></li>
                                    <li><a href="#"> Contact</a></li>
                                    <li><a href="#"> Appointment</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 col-lg-3">
                            <div class="footer_widget">
                                <h3 class="footer_title">
                                        Address
                                </h3>
                                <p>
                                    200, D-block, Green lane USA <br>
                                    +10 367 467 8934 <br>
                                    docmed@contact.com
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copy-right_text">
                <div class="container">
                    <div class="footer_border"></div>
                    <div class="row">
                        <div class="col-xl-12">
                            <p class="copy_right text-center">
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
<!-- footer end  -->
    <!-- link that opens popup -->

    <!-- form itself end-->
    <form id="test-form" class="white-popup-block mfp-hide">
        <div class="popup_box ">
            <div class="popup_inner">
                <h3>Make an Appointment</h3>
                <form action="#">
                    <div class="row">
                        <div class="col-xl-6">
                            <input id="datepicker" placeholder="Pick date">
                        </div>
                        <div class="col-xl-6">
                            <input id="datepicker2" placeholder="Suitable time">
                        </div>
                        <div class="col-xl-6">
                            <select class="form-select wide" id="default-select" class="">
                                <option data-display="Select Department">Department</option>
                                <option value="1">2</option>
                                <option value="2">3</option>
                                <option value="3">4</option>
                            </select>
                        </div>
                        <div class="col-xl-6">
                            <select class="form-select wide" id="default-select" class="">
                                <option data-display="Doctors">Doctors</option>
                                <option value="1">2</option>
                                <option value="2">3</option>
                                <option value="3">4</option>
                            </select>
                        </div>
                        <div class="col-xl-6">
                            <input type="text"  placeholder="Name">
                        </div>
                        <div class="col-xl-6">
                            <input type="text"  placeholder="Phone no.">
                        </div>
                        <div class="col-xl-12">
                            <input type="email"  placeholder="Email">
                        </div>
                        <div class="col-xl-12">
                            <button type="submit" class="boxed-btn3">Confirm</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </form>
    <!-- form itself end -->

    <!-- JS here -->
    <script src="{{ asset('template/js/vendor/modernizr-3.5.0.min.js') }} "></script>
    <script src="{{ asset('template/js/vendor/jquery-1.12.4.min.js') }} "></script>
    <script src="{{ asset('template/js/popper.min.js') }} "></script>
    <script src="{{ asset('template/js/bootstrap.min.js') }} "></script>
    <script src="{{ asset('template/js/owl.carousel.min.js') }} "></script>
    <script src="{{ asset('template/js/isotope.pkgd.min.js') }} "></script>
    <script src="{{ asset('template/js/ajax-form.js') }} "></script>
    <script src="{{ asset('template/js/waypoints.min.js') }} "></script>
    <script src="{{ asset('template/js/jquery.counterup.min.js') }} "></script>
    <script src="{{ asset('template/js/imagesloaded.pkgd.min.js') }} "></script>
    <script src="{{ asset('template/js/scrollIt.js') }} "></script>
    <script src="{{ asset('template/js/jquery.scrollUp.min.js') }} "></script>
    <script src="{{ asset('template/js/wow.min.js') }} "></script>
    <script src="{{ asset('template/js/nice-select.min.js') }} "></script>
    <script src="{{ asset('template/js/jquery.slicknav.min.js') }} "></script>
    <script src="{{ asset('template/js/jquery.magnific-popup.min.js') }} "></script>
    <script src="{{ asset('template/js/plugins.js') }} "></script>
    <script src="{{ asset('template/js/gijgo.min.js') }} "></script>
    <!--contact js-->
    <script src="{{ asset('template/js/contact.js') }} "></script>
    <script src="{{ asset('template/js/jquery.ajaxchimp.min.js') }} "></script>
    <script src="{{ asset('template/js/jquery.form.js') }} "></script>
    <script src="{{ asset('template/js/jquery.validate.min.js') }} "></script>
    <script src="{{ asset('template/js/mail-script.js') }} "></script>

    <script src="{{ asset('template/js/main.js') }} "></script>
    <script>
        $('#datepicker').datepicker({
            iconsLibrary: 'fontawesome',
            icons: {
                rightIcon: '<span class="fa fa-caret-down"></span>'
            }
        });
        $('#datepicker2').datepicker({
            iconsLibrary: 'fontawesome',
            icons: {
                rightIcon: '<span class="fa fa-caret-down"></span>'
            }

        });
    $(document).ready(function() {
    $('.js-example-basic-multiple').select2();
    });
    </script>
</body>

</html>