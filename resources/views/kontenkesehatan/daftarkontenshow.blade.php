@extends('adminLTE.master')
@push('style')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<!-- SweetAlert2 -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endpush

@section('title')
    <span>Daftar Konten Kesehatan</span>
@endsection

@section('content')
	<div class="container">
		@if (session('success'))
			<div class="alert alert-success">
				{{ session('success')}}
			</div>
		@endif
		<div class="btnatas d-flex justify-content-between">
			<a class="btn btn-primary mb-2 d-flex" href="/kontenkesehatan">+tambah</a>
		</div>
		<table class="table table-bordered">
		  <thead>
		    <tr>
		      <th style="width: 10px">id</th>
		      <th>judul</th>
			  <th>summary</th>
		      <th>isi</th>
		      <th>foto penunjang</th>
		      <th style="width: 40px">Aksi</th>
		    </tr>
		  </thead>
		  <tbody>		    
		    @forelse ($konten as $key => $value)
		    <tr>
		      <td>{{ $value->id }}</td>
		      <td>{{ $value->judul }}</td>
			  <td>{{ $value->summary }}</td>
		      <td>
		        {{Str::substr($value->isi , 0, 50) }} 
		      </td>
		      <td>
		      	@if(!empty($value->urlfoto))
		      	     <img src="{{ asset('uploads/'.$value->urlfoto) }}" style="height: 100px; width: 100px;" alt="foto">
		      	@else
		      	    <p>tidak ada foto</p>
		      	@endif		      	
		      </td>
		      <td class="d-flex">
		      	<a href="/kontenkesehatan/{{$value->id}}" class="btn btn-info btn-sm d-inline">Show</a>
		      	<a href="/kontenkesehatan/{{$value->id}}/edit" class="btn btn-default btn-sm d-inline ml-2">Edit</a>
		      	<a href="/kontenkesehatan/delete/{{$value->id}}" class="btn btn-danger ml-2 button delete-confirm">Delete</a>
				  {{-- <form action="/kontenkesehatan/{{$value->id}}" method="POST">
		      		@csrf
		      		@method('DELETE')
		      		<input type="submit" value="delete" class="btn btn-danger btn-sm ml-2" >

		      	</form> --}}
				  
		      </td>
		    </tr>
		    @empty
			<tr>
				<td colspan="6">No data</td>
			</tr>		    		
		    @endforelse
		  </tbody>
		</table>	
	</div>

@endsection
@push('script')
	<script>
		$('.delete-confirm').on('click', function (event) {
		event.preventDefault();
		const url = $(this).attr('href');
	swal({
        title: 'Are you sure?',
        text: 'This record and it`s details will be permanantly deleted!',
        icon: 'warning',
        buttons: ["Cancel", "Yes!"],
    }).then(function(value) {
        if (value) {
            window.location.href = url;
        }
    });
});
	</script>
@endpush
