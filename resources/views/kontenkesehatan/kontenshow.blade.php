@extends('adminLTE.master')

@section('title')
    <span>Detail Konten Kesehatan</span>
@endsection

@section('content')
	{{-- expr --}}
	<div class="container">
			{{-- expr --}}
		<h4>{{$konten->judul}}</h4>	
		<h2>{{$konten->summary}}</h2>
		{!! $konten->isi !!}
		@if (!empty($konten->urlfoto))
			<img src="{{ asset('uploads/'.$konten->urlfoto) }}" alt="foto">
		@endif

	</div>	
@endsection