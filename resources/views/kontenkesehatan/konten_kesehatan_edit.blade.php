@extends('adminLTE.master');
@push('style')
<script src="{{asset('ckeditor/ckeditor.js')}}"></script>

@endpush
@section('title')
	<h2>edit konten kesehatan</h2>
@endsection

@section('content')
		<div class="card card-primary">
	              <div class="card-header">
	                <h3 class="card-title">Edit Kontent</h3>                
	              </div>
	              <!-- /.card-header -->
	              <!-- form start -->
					@if (session('success'))
						<div class="alert alert-success">
							{{ session('success')}}
						</div>
					@endif
	              <form action="/kontenkesehatan/{{$konten->id}}" method="POST" enctype="multipart/form-data">
	              	@csrf
	              	@method('PUT')
	                <div class="card-body">
	                  <div class="form-group">
	                    <label for="judul">Judul</label>
	                    <input type="text" class="form-control" id="judul" name="judul" placeholder="masukkan judul" value="{{$konten->judul}}">
	                  </div>
	                  @error('judul')
	                      <div class="alert alert-danger">{{ $message }}</div>
	                  @enderror
					  <div class="form-group">
	                    <label for="judul">Summary</label>
	                    <input type="text" class="form-control" id="summary" name="summary" placeholder="masukkan summary" value="{{$konten->summary}}">
	                  </div>
	                  @error('judul')
	                      <div class="alert alert-danger">{{ $message }}</div>
	                  @enderror
	                  <div class="form-group">
	                        <label>Isi</label>
	                        <textarea id="konten" class="form-control" name="isi" rows="3" placeholder="isi konten">{{$konten->isi}}</textarea>
	                      </div>
	                      @error('judul')
	                          <div class="alert alert-danger">{{ $message }}</div>
	                      @enderror
	                  <div class="form-group">
	                    <label for="exampleInputFile">Foto penunjang ( 
							@if (!empty($konten->urlfoto))
								<a href="{{ asset('uploads/'.$konten->urlfoto) }}">{{$konten->urlfoto}} </a>
							@else
								<span>tidak ada foto</span>		
							@endif
	                    	)</label>
	                    <div class="input-group">
	                      <div class="custom-file">	            
	                        <input type="file" class="custom-file-input" id="exampleInputFile" name="foto">
	                        <label class="custom-file-label" for="exampleInputFile">ganti file</label>
	                      </div>
	                      <div class="input-group-append">
	                        <span class="input-group-text">Upload</span>
	                      </div>
	                    </div>
	                  </div>
	                </div>


	                <!-- /.card-body -->

	                <div class="card-footer">
	                  <input type="submit" class="btn btn-primary">
	                </div>
	              </form>
	            </div>
	
@endsection

@push('script')
	{{-- expr --}}
	<script>
		var konten = document.getElementById("konten");
		  CKEDITOR.replace(konten,{
		  language:'en-gb'
		});
		CKEDITOR.config.allowedContent = true;
	 </script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
@endpush