@extends('adminLTE.master')
@push('style')
<script src="{{asset('ckeditor/ckeditor.js')}}"></script>

@endpush
@section('title')
    <span>Create Konten Kesehatan</span>
@endsection

@section('content')
		<div class="card card-primary">
	              <div class="card-header">
	                <h3 class="card-title">Konten Kesehatan Baru</h3>                
	              </div>
	              <!-- /.card-header -->
	              <!-- form start -->
					@if (session('success'))
						<div class="alert alert-success">
							{{ session('success')}}
						</div>
					@endif
	              <form action="/kontenkesehatan" method="POST" enctype="multipart/form-data">
	              	@csrf
	                <div class="card-body">
	                  <div class="form-group">
	                    <label for="judul">Judul</label>
	                    <input type="text" class="form-control" id="judul" name="judul" placeholder="masukkan judul">
	                  </div>
	                  @error('judul')
	                      <div class="alert alert-danger">{{ $message }}</div>
	                  @enderror
					  <div class="form-group">
	                    <label for="summary">summary</label>
	                    <input type="text" class="form-control" id="summary" name="summary" placeholder="masukkan summary">
	                  </div>
	                  @error('judul')
	                      <div class="alert alert-danger">{{ $message }}</div>
	                  @enderror
					  <div class="form-group">
	                        <label>Isi</label>
	                        <textarea id="konten" class="form-control" name="isi" rows="3" placeholder="isi konten"></textarea>
	                      </div>
	                      @error('judul')
	                          <div class="alert alert-danger">{{ $message }}</div>
	                      @enderror
	                  <div class="form-group">
	                    <label for="exampleInputFile">Foto penunjang</label>
	                    <div class="input-group">
	                      <div class="custom-file">
	                        <input type="file" class="custom-file-input" id="exampleInputFile" name="foto">
	                        <label class="custom-file-label" for="exampleInputFile">pilih file</label>
	                      </div>
	                      <div class="input-group-append">
	                        <span class="input-group-text">Upload</span>
	                      </div>
	                    </div>
	                  </div>
	                </div>
	                <!-- /.card-body -->

	                <div class="card-footer">
	                  <input type="submit" class="btn btn-primary">
	                </div>
	              </form>
	            </div>
	
@endsection
@push('script')
<script>
	var konten = document.getElementById("konten");
	  CKEDITOR.replace(konten,{
	  language:'en-gb'
	});
	CKEDITOR.config.allowedContent = true;
 </script>
@endpush