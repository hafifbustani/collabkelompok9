@extends('adminLTE.master')

@section('title')
    <span>Edit Pasien</span>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8 ml-5 mt-3">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Pasien {{$query->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pasien/{{$query->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama_pasien">Nama Pasien</label>
                    <input type="text" class="form-control @error('nama_pasien') is-invalid @enderror" id="nama_pasien" name="nama_pasien" value="{{ old('nama_pasien', $query->nama_pasien) }}" placeholder="Enter Title">
                    @error('nama_pasien')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  
                  <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="text" class="form-control @error('umur') is-invalid @enderror" id="umur" name="umur" value="{{ old('umur', $query->umur) }}" placeholder="Enter Content">
                    @error('umur')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="token">Token pasien</label>
                    <input type="text" id="token" name="token" placeholder="Buat token" value="{{$query->token}}">
                  </div>

                  <fieldset class="form-group">
                    <div class="row">
                      <legend class="col-form-label col-sm-2 pt-0">Gender</legend>
                      <div class="col-sm-10">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="gender" id="P" value="{{ old('gender', 'P') }}">
                          <label class="form-check-label" for="P">
                            Perempuan
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="gender" id="L" value="{{ old('gender', 'L') }}">
                          <label class="form-check-label" for="L">
                            Laki-Laki
                          </label>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
@endsection