@extends('adminLTE.master')

@section('title')
    <span>Show Pasien</span>
@endsection

@section('content')
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">{{$query->nama_pasien}}</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
          <h4>Umur : {{$query->umur}}</h4>
          <p>Gender : {{$query->gender}}</p>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <a href="/pasien" class="btn btn-warning">Back</a>
        </div>
      </div>
@endsection