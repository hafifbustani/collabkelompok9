@extends('adminLTE.master')

@push('style')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<!-- SweetAlert2 -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endpush

@section('title')
    <span>Daftar Pasien</span>
@endsection

@section('content')
<div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Pasien</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if (session('status'))
                  <div class="alert alert-success">
                      {{ session('status') }}
                    </div>
                    @endif
                <a class="btn btn-primary mb-4" href="/pasien/create">Create New Pasien</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Nama Pasien</th>
                      <th>Umur</th>
                      <th>Gender</th>
                      <th>Token</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($pasien as $key => $value)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$value->nama_pasien}}</td>
                            <td>{{$value->umur}}</td>
                            <td>{{$value->gender}}</td>
                            <td>{{$value->token}}</td>
                            <td style="display: flex;">
                                <a class="btn btn-info btn-sm mr-2" href="/pasien/{{$value->id}}">Show</a>
                                <a class="btn btn-secondary btn-sm mr-2" href="/pasien/{{$value->id}}/edit">Edit</a>
                                <a href="/pasien/delete/{{$value->id}}" class="btn btn-danger ml-2 button delete-confirm">Delete</a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5" align="center">No Data</td>    
                        </tr>
                    @endforelse


                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
           </div>
        </div>
@endsection

@push('script')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
  $('.delete-confirm').on('click', function (event) {
      event.preventDefault();
      const url = $(this).attr('href');
      swal({
          title: 'Are you sure?',
          text: 'This record and it`s details will be permanantly deleted!',
          icon: 'warning',
          buttons: ["Cancel", "Yes!"],
      }).then(function(value) {
          if (value) {
              window.location.href = url;
          }
      });
  });
</script>
@endpush