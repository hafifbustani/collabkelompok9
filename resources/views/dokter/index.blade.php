@extends('adminLTE.master')

@section('title')
    <span>Daftar dokter</span>
@endsection

@section('content')
<div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Dokter</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if (session('status'))
                  <div class="alert alert-success">
                      {{ session('status') }}
                    </div>
                    @endif
                <a class="btn btn-primary mb-4" href="/dokter/create">Create New dokter</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Nama Dokter</th>
                      <th>Spesialis</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($dokter as $key => $value)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$value->nama_dokter}}</td>
                            <td>{{$value->specialist}}</td>
                            <td style="display: flex;">
                                <a class="btn btn-info btn-sm mr-2" href="/doctors">Show</a>
                                <a class="btn btn-secondary btn-sm mr-2" href="/dokter/{{$value->id}}/edit">Edit</a>
                                <form action="/dokter/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5" align="center">No Data</td>    
                        </tr>
                    @endforelse


                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
           </div>
        </div>
@endsection