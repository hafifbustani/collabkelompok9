@extends('adminLTE.master')

@section('title')
    <span>Daftar Dokter</span>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-8 ml-5 mt-3">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Create New Dokter</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          @if (session('success'))
                <div class="alert alert-success">
                  {{ session('success')}}
                </div>
              @endif
          <form role="form" action="/dokter" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="nama_pasien">Nama Dokter</label>
                <input type="text" class="form-control @error('nama_dokter') is-invalid @enderror" id="nama_dokter" name="nama_dokter" value="{{ old('nama_dokter', '') }}" placeholder="Masukan Nama">
                @error('nama_dokter')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>

              <div class="form-group">
                <label for="umur">Specialis</label>
                <input type="text" class="form-control @error('specialist') is-invalid @enderror" id="specialist" name="specialist" value="{{ old('specialist', '') }}" placeholder="Masukan spesialis">
                @error('specialist')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="form-group">
                <label for="exampleInputFile">Foto</label>
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="exampleInputFile" name="foto">
                    <label class="custom-file-label" for="exampleInputFile">pilih file</label>
                  </div>
                  <div class="input-group-append">
                    <span class="input-group-text">Upload</span>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Create</button>
              </div>
                    </div>
                  </div>
                </div>
              </fieldset>
            </div>

@endsection