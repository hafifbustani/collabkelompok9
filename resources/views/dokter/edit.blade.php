@extends('adminLTE.master')

@section('title')
    <span>Edit Pasien</span>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8 ml-5 mt-3">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit dokter {{$query->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/dokter/{{$query->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama_dokter">Nama dokter</label>
                    <input type="text" class="form-control @error('nama_pasien') is-invalid @enderror" id="nama_dokter" name="nama_dokter" value="{{ old('nama_dokter', $query->nama_dokter) }}" placeholder="Enter dokter">
                    @error('nama_dokter')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  
                  <div class="form-group">
                    <label for="specialist">specialist</label>
                    <input type="text" class="form-control @error('specialist') is-invalid @enderror" id="specialist" name="specialist" value="{{ old('specialist', $query->specialist) }}" placeholder="Enter spsialis">
                    @error('specialist')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  
                  <input type="submit" class="btn btn-primary" value="Update">
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
@endsection