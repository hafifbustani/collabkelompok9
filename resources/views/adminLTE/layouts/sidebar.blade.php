<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="/" class="brand-link">
    <img src="{{asset('adminLTE/dist/img/AdminLTELogo.png')}}"
         alt="AdminLTE Logo"
         class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">Project Klinik</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{asset('adminLTE/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">User</a>
        {{-- <a href="#" class="d-block">{{ Auth::user()->pasien->name }}</a> --}}
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        @if (Auth::user('isadmin'))
        <li class="nav-item has-treeview menu-open">
          <a href="#" class="nav-link">
            <i class="nav-icon far fa-plus-square"></i>
            <p>
              Pasien
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/pasien" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Pasien</p>
              </a>
            </li>
          </ul>
        </li>            
        <li class="nav-item has-treeview menu-open">
          <a href="#" class="nav-link">
            <i class="nav-icon far fa-plus-square"></i>
            <p>
              Konten Kesehatan
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/kontenkesehatan" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Konten Kesehatan</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/kontenkesehatanshow" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Show Konten Kesehatan</p>
              </a>
            </li>              
            </li>
          </ul>
          
        </li>
        <li class="nav-item has-treeview menu-open">
          <a href="#" class="nav-link">
            <i class="nav-icon far fa-plus-square"></i>
            <p>
              Dokter
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/dokter/create" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Dokter</p>
              </a>
              <li class="nav-item">
                <a href="/dokter" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Daftar dokter</p>
                </a>
              </li>
            </li>
            </li>
          </ul>
        </li>
        @endif

        <li class="nav-item has-treeview menu-open">
          <a href="#" class="nav-link">
            <i class="nav-icon far fa-plus-square"></i>
            <p>
              Appointment
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/dokterhaspasien" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Buat Janji Dengan Dokter</p>
              </a>
            </li>
            @if (Auth::user('isadmin'))
            <li class="nav-item">
              <a href="/listpasienr" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Lihat pasien terdaftar</p>
              </a>
            </li>
            </li>
          </ul>
        </li>    
            @endif
            
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>