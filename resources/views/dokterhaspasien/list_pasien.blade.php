@extends('adminLTE.master')

@section('headtitle')
    Pasien terdaftar dan jadwal periksa
@endsection

@section('title')
	Pasien terdaftar
@endsection

@push('style')
	{{-- expr --}}
	 <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	  <link rel="stylesheet" href="{{ asset('/adminLTE/plugins/fontawesome-free/css/all.min.css') }}">
	  <link rel="stylesheet" href="{{ asset('/adminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
	  <link rel="stylesheet" href="{{ asset('/adminLTE/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
	  <link rel="stylesheet" href="{{ asset('/adminLTE/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
	  <!-- Theme style -->
	  <link rel="stylesheet" href="{{ asset('/adminLTE/dist/css/adminlte.min.css') }}">
@endpush

@section('content')
	{{-- expr --}}
	<div class="container">
		<div class="card-body">
            <form action="/showlistpasienbydokter" method="POST">
            @csrf
            <label for="id_dokter">Pilih Dokter:</label><select name="id_dokter" id="id_dokter" class="">
                <option value="#" selected>select</option>
            @isset($alldokter)
                @foreach ($alldokter as $item=>$value)
                <option value="{{$value->id}}">{{$value->nama_dokter}}</option>    
                @endforeach
            @endisset 
            <input type="submit" class="btn btn-info btn-sm ml-2">            
        </select>
            </form>
            @isset($selecteddokter)
                <h4>daftar pasien dr. {{$selecteddokter}} </h4>
            @endisset            
		  <table id="example1" class="table table-bordered table-striped">
		    <thead>
		    <tr>
		      <th>Nama Dokter</th>
		      <th>Spesialisasi</th>
		      <th>Nama Pasien</th>
		      <th>Waktu Periksa</th>
		    </tr>
		    </thead>
		    <tbody>
				@isset($dokter)
				@foreach ($dokter->pasien as $item)                               
                <tr role="row" class="odd">            
                <td class="dtr-control sorting_1" tabindex="0">{{$item->pivot->nama_dokter}}</td>
                <td class="dtr-control sorting_1" tabindex="1">{{$item->pivot->spesialisasi}}</td>
                <td class="dtr-control sorting_1" tabindex="2">{{$item->nama_pasien}}</td>
                <td class="dtr-control sorting_1" tabindex="3">{{$item->pivot->waktu_periksa}}</td>
                </tr>
				@endforeach									
				@else
				<p>silahkan pilih dokter </p>
				@endisset
                  
		    			
		    </tbody>
		  </table>
		</div>
		
	</div>
@endsection

@push('script')
	{{-- expr --}}
	<script src="{{ asset('adminLTE/plugins/jquery/jquery.min.js') }}"></script>
	<!-- Bootstrap 4 -->
	<script src="{{ asset('adminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<!-- DataTables  & Plugins -->
	<script src="{{ asset('adminLTE/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('adminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('adminLTE/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('adminLTE/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('adminLTE/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('adminLTE/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('adminLTE/plugins/jszip/jszip.min.js') }}"></script>
	<script src="{{ asset('adminLTE/plugins/pdfmake/pdfmake.min.js') }}"></script>
	<script src="{{ asset('adminLTE/plugins/pdfmake/vfs_fonts.js') }}"></script>
	<script src="{{ asset('adminLTE/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('adminLTE/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
	<script src="{{ asset('adminLTE/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('adminLTE/dist/js/adminlte.min.js') }}"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="{{ asset('adminLTE/dist/js/demo.js') }}"></script>

	<script>
	  $(function () {
	    $("#example1").DataTable({
	      "responsive": true, "lengthChange": false, "autoWidth": false,
	      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
	    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
	    $('#example2').DataTable({
	      "paging": true,
	      "lengthChange": true,
	      "searching": true,
	      "ordering": true,
	      "info": true,
	      "autoWidth": true,
	      "responsive": true,
	    });
	  });
	</script>

@endpush