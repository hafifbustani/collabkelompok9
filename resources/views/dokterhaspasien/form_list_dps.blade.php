@extends('adminLTE.master');
@push('style')
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 

<!-- Inline CSS based on choices in "Settings" tab -->
<style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form{font-family: Arial, Helvetica, sans-serif; color: black}.bootstrap-iso form button, .bootstrap-iso form button:hover{color: white !important;} .asteriskField{color: red;}</style>

@endpush
@section('title')
    Form list daftar pasien
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-8 ml-5 mt-3">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Form Buat Janji dengan Dokter</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          @if (session('success'))
						<div class="alert alert-success">
							{{ session('success')}}
						</div>
					@endif
          @if (session('gagal'))
						<div class="alert alert-danger">
							{{ session('gagal')}}
						</div>
					@endif
          <form role="form" action="/dpsstore" method="POST">
            @csrf
            <div class="form-group">               
                @error('nama_pasien')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="container">
              <div class="form-group">
                <div class="form-group">
                    <label>Nama Pasien</label>
                    <select class="custom-select" name="nama_pasien">                    
                    @forelse ($daftarpasien as $itempasien=>$valuepasien)
                        <option value="{{$valuepasien->id}}">{{$valuepasien->nama_pasien}}.{{$valuepasien->gender}}.({{$valuepasien->umur}})</option>
                    @empty
                        <option value="">Tidak ada data pasien</option>
                    @endforelse
                </select>    
                    
                  </div>
                </div>
                
              </div>
            <div class="card-body">
                <div class="form-group">
                  <label for="nama_dokter">Pilih Dokter</label>
                  <select name="nama_dokter" id="nama_dokter">
                    @forelse ($daftardokter as $key => $value)
                    <option value="{{$value->id}}">{{$value->nama_dokter}}</option>  
                    @empty
                     <option value="">-tidak ada data dokter-</option>   
                    @endforelse
                  </select>
                  @error('nama_dokter')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>                                  
                <div class="form-group">
                  <label for="token">Masukkan Tokken Pasien</label>
                  <input type="text" id="token" name="token" placeholder="masukkan token">
                </div> 
            </div>
            <div class="bootstrap-iso">
                <div class="container-fluid">
                 <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12">                   
                    <div class="form-group ">
                     <label class="control-label " for="date">
                      Tanggal Periksa
                     </label>
                     <input class="form-control" id="date" name="date" placeholder="tanggal periksa" type="text"/>
                    </div>
                    <div class="form-group">                     
                    </div>                  
                  </div>
                 </div>
                </div>
               </div>
            <!-- /.card-body -->
            </fieldset>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Daftarkan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
</div>
@endsection
@push('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script>
    $(document).ready(function(){
        var date_input=$('input[name="date"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
    })
</script>
@endpush