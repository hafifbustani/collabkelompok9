<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function() {
//     return view('adminindex'); //menambah admin index (HAFIF)
// });

Route::get('/', 'DokterController@indexhome');
Route::get('/departement', function () {
    return view('department');
});
Route::get('/blog', 'KontenKesehatanController@frontendkonten');
Route::get('/doctors', 'DokterController@frontenddokter');
Route::get('/contact', function () {
    return view('contact');
});
Route::get('admin',['middleware' => 'isadmin', function () {
    return view('adminindex');
}]);
Route::get('user', ['middleware' => 'auth', function () {
    return view('userindex');
}]);
// Route::get('dokter', function () {
//     return view('dokter.index');
// });

Auth::routes();

//Route Konten kesehatan (HAFIF)
Route::middleware(['isadmin'])->group(function () {
// Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/pasien', 'PasienController@index');
    Route::get('/pasien/create', 'PasienController@create');
    Route::post('/pasien', 'PasienController@store');
    Route::get('/pasien/{id}', 'PasienController@show');
    Route::get('/pasien/{id}/edit', 'PasienController@edit');
    Route::put('/pasien/{id}', 'PasienController@update');
    Route::get('/pasien/delete/{id}', 'PasienController@destroy');

    Route::get('/dokter','DokterController@index');
    Route::get('/dokter/create', 'DokterController@create');
    Route::post('/dokter', 'DokterController@post');
    Route::get('/dokter/{id}', 'DokterController@show');
    Route::get('/dokter/{id}/edit', 'DokterController@edit');
    Route::put('/dokter/{id}', 'DokterController@update');
    Route::delete('/dokter/{id}', 'DokterController@destroy');
    Route::get('/kontenkesehatan', 'KontenKesehatanController@create');
    Route::post('/kontenkesehatan', 'KontenKesehatanController@store');
    Route::get('/kontenkesehatanshow','KontenKesehatanController@index');
    Route::get('/kontenkesehatan/{id}', 'KontenKesehatanController@show');    
    Route::get('/kontenkesehatan/{id}/edit', 'KontenKesehatanController@edit');
    Route::put('/kontenkesehatan/{id}', 'KontenKesehatanController@update');
    Route::delete('/kontenkesehatan/{id}', 'KontenKesehatanController@destroy');
    Route::get('/kontenkesehatan/delete/{id}', 'KontenKesehatanController@destroy2');
    //Route many to many (Hafif)   
    Route::get('/listpasienr', 'DokterPasienController@showlistpasien');
    Route::post('/showlistpasienbydokter', 'DokterPasienController@showlistpasienbydokter');        
});
Route::get('/dokterhaspasien', 'DokterPasienController@dpsindex');
Route::post('/dpsstore', 'DokterPasienController@dpsstore');
Route::get('kontenkesehatandetails/{id}', 'KontenKesehatanController@fshow');

