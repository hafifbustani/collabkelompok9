<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToDokterPasien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dokter_pasien', function (Blueprint $table) {
            $table->string('nama_dokter');
            $table->string('spesialisasi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dokter_pasien', function (Blueprint $table) {
            $table->dropColumn('nama_dokter');
            $table->dropColumn('spesialisasi');
        });
    }
}
