## Project

Pengumpulan Final Project melalui dasbor sanbercode (tiap peserta mengumpulkan di dasbor nya masing-masing , Bukan perwakilan kelompok).

Link yang dikumpulkan adalah link repositori git nya.
Pastikan repositori nya public agar dapat dilihat oleh trainer.

Cantumkan informasi berikut di file README.md :

1. Informasi kelompok (nomor kelompok, anggota, tema project yang diangkat)
2. Link video demo yang berisi penjelasan singkat mengenai project yang dikerjakan (mencakup ERD, template yang dipakai, code, demo web)
3. Link deploy di hosting sendiri atau heroku (opsional).

Pengumpulan paling telat Hari Jum'at 12 Maret 2021 jam 23.59 WIB.

### Project Tugas Kelompok 9

    (1).Tema Project : WebBlog Klinik
    (2). Template yang digunakan : Backend = AdminLTE, Frontend = DOCMED
    (3). Library yang digunakan :
        1.Sweet Alert
        2.Data Tables
        3.CK Editor
    (4). Jika ingin mengakses backend harus register user dahulu

#### Nama Kelompok :

1.Roby Afrizal Palmendha (@robyafrizal)

2.Hafif Bustani Wahyudi (@hafifbustani)

3.Primas pranata (@emjetaz)

### [link ke video Demo](https://www.youtube.com/watch?v=jRXMPZRzzSc)

#### Link ERD = <img src=" https://i.ibb.co/n85WB9G/finalERD.png" alt="ERD">

### [link deploy](http://www.eklinikdemo.xyz/)
